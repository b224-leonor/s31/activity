let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == "/"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("The server is successfully running")
	}
	if(request.url == "/login"){
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("You are in login page")
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("This page doesn't exist");
	}

});
server.listen(port);
console.log(`Server is now running at localhost: ${port}.`);